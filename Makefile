.POSIX:

include config.mk

all:

install:
#	Install executable
	mkdir -p $(DESTDIR)$(BINDIR)
	awk -F'#' -f util/process-macros.awk \
		-v 'libdir=$(DESTDIR)$(LIBDIR)/$(PROGNAME)' $(PROGNAME) \
		> $(DESTDIR)$(BINDIR)/$(PROGNAME)
	chmod 755 $(DESTDIR)$(BINDIR)/$(PROGNAME)
	
#	Install libraries
	mkdir -p $(DESTDIR)$(LIBDIR)/$(PROGNAME)
	cp lib/*.lua $(DESTDIR)$(LIBDIR)/$(PROGNAME)
	chmod 644 $(DESTDIR)$(LIBDIR)/$(PROGNAME)/*

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/$(PROGNAME)
	rm -rf $(DESTDIR)$(LIBDIR)/$(PROGNAME)

.PHONY: all install uninstall
