# username-tool

Generate a random username from a dictionary wordlist file.

## Dependencies

  - Lua 5.4
  - POSIX make (build)

A wordlist file is needed for this program to work. Word lists can be
downloaded from <http://wordlist.aspell.net/>. Make sure to edit the
configuration file to point to your word list.

### Debian

	apt-get install lua5.4 make

To get a wordlist file, you can install the `wamerican` package, or any other
word list package, such as `wamerican-small` or `wbritish`. Word lists appear
in the `/usr/share/dict` directory.

## Installation

Edit `config.mk`, and then run:

	make install

## Configuration

`username-tool` looks for a configuration file in
`$XDG_CONFIG_HOME/username-tool/config.lua` or
`$HOME/.config/username-tool/config.lua` if `$XDG_CONFIG_HOME` is unset.
There's an example configuration file in `dist/config.lua` with the default
values for each parameter.

## Usage

Execute `username-tool` without arguments to get a brief usage message.

Licenced under the MIT License, see `LICENSE`.
