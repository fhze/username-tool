#!/usr/bin/lua5.4

local lfs = require "lfs"

-- #ifndef RELEASE
local function realfiledir(path)
    local oldcwd = assert(lfs.currentdir())

    local reldir = string.gsub(path, "/[^/]*$", "")
    assert(lfs.chdir(reldir))
    local dirpath = assert(lfs.currentdir())

    assert(lfs.chdir(oldcwd))
    return dirpath
end

local libdir = realfiledir(arg[0]) .. "/lib"
-- #endif

local function loadlib(path)
    local name = string.match(path, "lib%.(.*)")
    if not name then
        return nil
    end

    local path = libdir .. "/" .. name .. ".lua"

    local function loader()
        return dofile(path)
    end

    -- Lua 5.4 documentation says that the second argument returned by the
    -- search function will be passed to the loader, but that doesn't seem to
    -- be the case, so we just use the upvalue.
    return loader
end

table.insert(package.searchers, 1, loadlib)

local main = require "lib.main"

return main.main(arg)
