local M = {}

local util = require "lib.util"
local generator = require "lib.generator"

conf = {
    wordlist_path = "/usr/share/dict/words"
}

--[[
local function arginteger(args, n, is_unsigned)
    local arg = args[n]
    local pattern, kind
    if is_unsigned then
        pattern = "^%d+$"
        kind = "unsigned integer"
    else
        pattern = "^%-?%d+$"
        kind = "integer"
    end
    
    if type(arg) ~= "string" then
        util.dief("Bad argument #%d (expected %s)", n, kind)
    end

    if not string.match(arg, pattern) then
        util.dief("Bad argument #%d (expected %s)", n, kind)
    end

    return assert(tonumber(arg))
end
--]]

local function usage()
    io.stdout:write(string.format([=[
Usage: %s <action> <args>

Actions:
  generate username [<count>]
]=], arg0))
    os.exit(1)
end

local function get_config_path()
    local xdg_config_home = os.getenv("XDG_CONFIG_HOME")
    if xdg_config_home ~= nil then
        return xdg_config_home.."/username-tool/config.lua"
    else
        local home = assert(os.getenv("HOME"))
        return home.."/.config/username-tool/config.lua"
    end
end


local function init()
    arg0 = arg[0]
    math.randomseed(os.time())
    generator.init()

    local config_path = get_config_path()
    local chunk = loadfile(config_path, "t")
    if chunk then
        chunk()
    end
end

local actionlist = {
    generate = {
        "username"
    }
}

function M.main(arg)
    init()

    if arg[1] == "generate" then
        if arg[2] == "username" then
            local count = 1
            if arg[3] then
                if not string.match(arg[3], "^%d+$") then
                    usage()
                end
                count = tonumber(arg[3])
            end

            for i=1, count do
                print(generator.generate_username())
            end
        else
            usage()
        end
    else
        usage()
    end
end

return M
