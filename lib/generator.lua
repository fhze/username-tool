local M = {}

local words = {}

local function random_word()
    local word
    while true do
        local i = math.random(#words)
        word = words[i]
        if string.match(word, "^%a+$") then
            return word
        end
    end
end

local function double_word_generator()
    local word1 = random_word()
    local word2 = random_word()
    return word1.."_"..word2
end

local generators = {
    double_word_generator
}

function M.init()
    local wordlist_file = assert(io.open(conf.wordlist_path, "r"))
    while true do
        local word = wordlist_file:read("l")
        if word == nil then
            break
        end
        table.insert(words, word)
    end
    wordlist_file:close()
end

function M.generate_username()
    local sel = math.random(#generators)
    local generator = generators[sel]

    return generator()
end

return M
