local M = {}

function M.errf(...)
    local formattable = {...}
    table.remove(formattable, 1)
    local level = ...
    local message = string.format(table.unpack(formattable))

    error(message, level + 1)
end

return M
