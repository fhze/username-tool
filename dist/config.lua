-- Configuration file for username-tool with the default values for each
-- property.

-- Where to look for a word list file, when generating an username from random
-- words.
conf.wordlist_path = "/usr/share/dict/words"
